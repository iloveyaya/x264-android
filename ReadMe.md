使用说明：
针对android摄像头，使x264支持NV21格式，编码参数指定i_csp = X264_CSP_NV21即可。

基于https://github.com/dreifachstein/x264
在其基础上，修改了x264_picture_alloc函数，添加了X264_CSP_NV21支持。